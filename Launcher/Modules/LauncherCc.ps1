<# Script Launcher créé par Samuel PAGES 

Liens entre autre :

        https://adminscache.wordpress.com/
        https://cod-box.net/programmation-pour-les-debutants/apprendre-powershell/article-apprendre-powershel-pour-les-debutante/powershell-navigation-dans-les-repertoires-et-le-fichiers/#:~:text=Powershell%20%3A%20Afficher%20le%20contenue%20d,dans%20le%20syst%C3%A8me%20de%20fichiers.
        https://adminscache.wordpress.com/2014/08/03/powershell-winforms-menu/
        https://www.systanddeploy.com/2020/09/build-powershell-systray-tool-with.html
        https://docs.microsoft.com/fr-fr/dotnet/api/system.windows.forms.progressbarstyle?view=net-5.0
        https://docs.microsoft.com/fr-fr/dotnet/api/system.windows.forms.progressbar.style?view=net-5.0
        https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.toolstripprogressbar?view=net-5.0
        https://docs.microsoft.com/en-us/dotnet/desktop/winforms/controls/toolstripprogressbar-control-overview?view=netframeworkdesktop-4.8
        https://mcpmag.com/articles/2014/02/18/progress-bar-to-a-graphical-status-box.aspx
        https://blog.adamfurmanek.pl/2016/03/19/executing-c-code-using-powershell-script/
        https://qastack.fr/programming/4016451/can-powershell-run-commands-in-parallel
        https://www.reddit.com/r/PowerShell/comments/d0e4cy/service_as_system_tray_icon/
        https://www.systanddeploy.com/2020/09/build-powershell-systray-tool-with.html
        https://gist.github.com/darrenjrobinson/afd88b2ea1ab0fed26290aee0411f649
        https://www.google.com/search?q=powershell+system.windows.forms.menuitem+icon&sxsrf=AOaemvIUF6lTYluVfB_07avKAYaTC3oAhQ:1631555762984&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj-2LvtwvzyAhUyxoUKHaeJCDoQ_AUoAXoECAIQAw&biw=1920&bih=937#imgrc=ciO3foANNjtyaM
        http://eddiejackson.net/wp/?p=19041
        https://blog.enguerrand.pro/2018/05/powershell-creation-dinterfaces.html
        https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.toolstripitem.image?redirectedfrom=MSDN&view=net-5.0#System_Windows_Forms_ToolStripItem_Image
        https://blog.enguerrand.pro/2018/05/powershell-creation-dinterfaces_11.html
        https://stackoverflow.com/questions/28481811/how-to-correctly-check-if-a-process-is-running-and-stop-it
        https://cnf1g.com/afficher-un-messagebox-en-powershell/
        https://smsagent.blog/2017/08/24/a-customisable-wpf-messagebox-for-powershell/
        https://www.it-swarm-fr.com/fr/powershell/progression-lors-de-la-copie-de-fichiers-volumineux-copy-item-write-progress/968067289/
        https://www.powershellgallery.com/packages/BurntToast/0.5.2/Content/Public%5CNew-BTButton.ps1
        https://www.powershellgallery.com/packages/BurntToast/0.7.0/Content/lib%5CMicrosoft.Toolkit.Uwp.Notifications%5CMicrosoft.Toolkit.Uwp.Notifications.xml
        https://forum-windows7-windows8.fr/viewtopic.php?t=12019
        https://www.powershell-scripting.com/index.php/forum/5-entraide-pour-les-debutants/29889-metadata-mp3
        https://www.it-connect.fr/afficher-une-notification-sur-windows-10-avec-powershell/
#>

Start-Transcript -Path ".\Launcher.log" -Force -Confirm:$false -Append:$false -ErrorAction Continue

# // LAUNCHER \\
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

$Form1 = New-Object System.Windows.Forms.Form
$Form1.Text = " LauncherOfGamer"
$Form1.Size = New-Object System.Drawing.Size(1152,710)
$Form1.ShowInTaskbar = $true
$Form1.StartPosition = "CenterScreen"
$Form1.MinimizeBox = $true
$Form1.MaximizeBox = $false
$Form1.ControlBox = $true
$Form1.FormBorderStyle = 3
$Form1.Opacity = 1
$Form1.UseWaitCursor = $false
$Form1.AutoScroll = $false
$Form1.HorizontalScroll.Enabled = $false
$Form1.VerticalScroll.Enabled = $false
$Form1.VerticalScroll.Visible = $false
$Form1.Topmost = $false
$Form1.MaximumSize = "1152,710" 
$Form1.MinimumSize = "1152,710"
$Form1.SizeGripStyle = 2
$Form1.Capture = $false
$Form1.KeyPreview = $false
$Form1.AllowTransparency = $false
$Form1.AllowDrop = $false

$ScriptDir = $PSScriptRoot

# // Images \\
$objImageBack = [system.drawing.image]::FromFile("$ScriptDir\Images\Back.jpg")
$objImageDraw = [system.drawing.image]::FromFile("$ScriptDir\Images\Dessin.bmp")
$objImageAbout = [system.drawing.image]::FromFile("$ScriptDir\Images\AProp.png")
$objImageRW = [system.drawing.image]::FromFile("$ScriptDir\Images\RW.jpg")
$objImageAL = [system.drawing.image]::FromFile("$ScriptDir\Images\AL.jpg")
$objImageR6 = [system.drawing.image]::FromFile("$ScriptDir\Images\R6.jpg")
$objImageGLF = [system.drawing.image]::FromFile("$ScriptDir\Images\GLF.jpg")
$objImageGTA = [system.drawing.image]::FromFile("$ScriptDir\Images\GTA-V.jpg")
$objNuit = [system.drawing.image]::FromFile("$ScriptDir\Images\Nuit.jpg")
$objImageSteel = [system.drawing.image]::FromFile("$ScriptDir\Images\Steel.png")
$objImageHMM = [system.drawing.image]::FromFile("$ScriptDir\Images\HMM.png")
$objImageAOE = [system.drawing.image]::FromFile("$ScriptDir\Images\AOE.jpg")
$objImageOF = [system.drawing.image]::FromFile("$ScriptDir\Images\OF.jpg")
$objImageRE = [system.drawing.image]::FromFile("$ScriptDir\Images\RE.png")
$objImageGE = [system.drawing.image]::FromFile("$ScriptDir\Images\GPO.png")
$objImageALL = [system.drawing.image]::FromFile("$ScriptDir\Images\ALL.jpg")
$objImageDD = [system.drawing.image]::FromFile("$ScriptDir\Images\DD.png")
$objImageTVW = [system.drawing.image]::FromFile("$ScriptDir\Images\TVW.png")
$objImageHW = [system.drawing.image]::FromFile("$ScriptDir\Images\HW.png")
$objImageWp = [system.drawing.image]::FromFile("$ScriptDir\Images\Wamp.png")
$objImageSC = [system.drawing.image]::FromFile("$ScriptDir\Images\SC.png")
$objImageSL = [system.drawing.image]::FromFile("$ScriptDir\Images\SL.png")
$objImageMC = [system.drawing.image]::FromFile("$ScriptDir\Images\MC.png")
$objImageON = [system.drawing.image]::FromFile("$ScriptDir\Images\OneNote.png")
$objImageEx = [system.drawing.image]::FromFile("$ScriptDir\Images\Excel.png")
$objImageW = [system.drawing.image]::FromFile("$ScriptDir\Images\Word.png")
$objImageSUITE = [system.drawing.image]::FromFile("$ScriptDir\Images\SUITE.jpg")
$objImageYT = [system.drawing.image]::FromFile("$ScriptDir\Images\YT.png")
$objImageT = [system.drawing.image]::FromFile("$ScriptDir\Images\Twitch.jpg")
$objImageSTS = [system.drawing.image]::FromFile("$ScriptDir\Images\STS.png")
$objImageVC = [system.drawing.image]::FromFile("$ScriptDir\Images\VSCode.png")
$objImageSt = [system.drawing.image]::FromFile("$ScriptDir\Images\Steam.jpg")
$objImageSi = [system.drawing.image]::FromFile("$ScriptDir\Images\Signal.png")
$objImageLively = [system.drawing.image]::FromFile("$ScriptDir\Images\Lively.png")
$objImageEg = [system.drawing.image]::FromFile("$ScriptDir\Images\EG.jpg")
$objImageBa = [system.drawing.image]::FromFile("$ScriptDir\Images\Battle.jpg")
$objImageM = [system.drawing.image]::FromFile("$ScriptDir\Images\minecraft.jpg")
$objImageF = [system.drawing.image]::FromFile("$ScriptDir\Images\Faceit.png")
$objImageCh = [system.drawing.image]::FromFile("$ScriptDir\Images\Chrome.jpg")
$objImageDisc = [system.drawing.image]::FromFile("$ScriptDir\Images\Discord.png")
$objImageS = [system.drawing.image]::FromFile("$ScriptDir\Images\Spotify.png")
$objImageO = [system.drawing.image]::FromFile("$ScriptDir\Images\Overwatch.png")
$objImageV = [system.drawing.image]::FromFile("$ScriptDir\Images\Valorant.jpg")
$objImageD3 = [system.drawing.image]::FromFile("$ScriptDir\Images\Diablo3.png")
$objImageL = [system.drawing.image]::FromFile("$ScriptDir\Images\LoL.jpg")
$objImageC = [system.drawing.image]::FromFile("$ScriptDir\Images\CSGO.jpg")
$objImageR = [system.drawing.image]::FromFile("$ScriptDir\Images\RocketLeague.jpg")

#[System.Windows.Forms.MenuStrip]$mainMenu=New-Object System.Windows.Forms.MenuStrip; 
#$form1.Controls.Add($mainMenu);

$objImage = [system.drawing.image]::FromFile("$ScriptDir\Images\Background.bmp")

$Form1.BackgroundImage = $objImage
$objIcon = New-Object system.drawing.icon ("$ScriptDir\Icones\Logo.ico")
$AboutIco = New-Object system.drawing.icon ("$ScriptDir\Icones\Question-bleu.ico")
$Form1.Icon = $objIcon

$Date = [System.DateTime]::Now
if ($Date -ne (Get-Date)) {
    $Date = Get-Date -ErrorAction Continue
}

function About {
    # About Form Objects
    $statusLabel = New-Object System.Windows.Forms.ToolStripStatusLabel
    $statusLabel.Text = "About"
    $aboutForm = New-Object System.Windows.Forms.Form
    $aboutFormExit = New-Object System.Windows.Forms.Button
    $aboutFormImage = New-Object System.Windows.Forms.PictureBox
    $aboutFormNameLabel = New-Object System.Windows.Forms.Label
    $aboutFormText = New-Object System.Windows.Forms.Label
 
    # About Form
    $aboutForm.AcceptButton = $aboutFormExit
    $aboutForm.CancelButton = $aboutFormExit
    $aboutForm.ClientSize = "410, 120"
    $aboutForm.MinimumSize = "410, 120"
    $aboutForm.MaximumSize = "410, 120"
    $aboutForm.ControlBox = $true
    $aboutForm.MinimizeBox = $false
    $aboutForm.MaximizeBox = $false
    $aboutForm.Icon = $AboutIco
    $aboutForm.ShowInTaskBar = $false
    $aboutForm.StartPosition = "CenterScreen"
    $aboutForm.Text = "A propos de Launcher .exe/.ps1 "
    $aboutForm.Add_Load($aboutForm_Load)
 
    # About PictureBox
    $aboutFormImage.Image = $objIcon
    $aboutFormImage.Location = "25, 15"
    $aboutFormImage.Size = "32, 32"
    $aboutFormImage.SizeMode = "StretchImage"
    $aboutForm.Controls.Add($aboutFormImage)
 
    # About Name Label
    $aboutFormNameLabel.Font = New-Object Drawing.Font("Microsoft Sans Serif", 9, [System.Drawing.FontStyle]::Bold)
    $aboutFormNameLabel.Location = "75, 20"
    $aboutFormNameLabel.Size = "200, 18"
    $aboutFormNameLabel.Text = "Launcher Credits"
    $aboutForm.Controls.Add($aboutFormNameLabel)
 
    # About Text Label
    $aboutFormText.Location = "80, 40"
    $aboutFormText.Size = "320, 40"
    $aboutFormText.Text = " Créé par Samuel PAGES `n`r Logs : $ScriptDir\Launcher.log "
    $aboutForm.Controls.Add($aboutFormText)
 
    # About Exit Button
    $aboutFormExit.Location = "135, 90"
    $aboutFormExit.Text = "OK"
    $aboutForm.Controls.Add($aboutFormExit)
    $tetris = Start-Process "$ScriptDir\Modules\Tetris.ps1" -WindowStyle Hidden
    [void]$aboutForm.ShowDialog()
    $statusLabel.Text = "Ready"
} # End About

function Draw {

        $Header = New-BTHeader -Id '005' -Title 'Draw'
        $BouttonLOG1 = New-BTButton -Content 'Aide Draw' -Arguments 'https://onedrive.live.com/?authkey=%21ABqDJoCB6eySaLw&cid=18784FF713CE61C1&id=18784FF713CE61C1%21177&parId=18784FF713CE61C1%21105&o=OneUp'
        $BouttonLOG2 = New-BTButton -Content 'PowerShell Random Geometric Patterns v2' -Arguments 'https://adminscache.wordpress.com/' 

        $Splat = @{
            Text = 'Le launcher s est bien éteint !',
                    'Cliques sur un des boutons',
                    'si tu souhaites le l aide'

            Sound = 'Mail'
            Header = $Header
            AppLogo = "$ScriptDir\Icones\RondRouge.ico"
            Button = $BouttonLOG1, $BouttonLOG2

        }
        New-BurntToastNotification @Splat

    PowerShell.exe -windowstyle hidden "$ScriptDir\Modules\RandomDraws.ps1" 
}

# // Notification de lancement \\ 

$NotifyIcon= New-Object System.Windows.Forms.NotifyIcon

$iconLaunch = New-Object System.Drawing.Icon("$ScriptDir\Icones\Logo.ico")
$date = [System.DateTime]::Now

$NotifyIcon.Icon =  $iconLaunch
$NotifyIcon.Text = "LauncherOfGamer"
$NotifyIcon.Visible = $True
$NotifyIcon.

[System.GC]::Collect()

$Contextmenu = New-Object System.Windows.Forms.ContextMenuStrip

$Menu1 = $Contextmenu.Items.Add("Apps");

$Menu5 = $contextmenu.Items.Add("RTX");
$Menu5.add_Click({
    Start-Process "https://www.amazon.fr/s?k=RTX3080&__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss_2"
    Start-Process "https://www.ldlc.com/recherche/RTX3080/"
    Start-Process "https://www.rueducommerce.fr/rayon/composants-16/nvidia-geforce-rtx-3080-124303"
    Start-Process "https://www.cybertek.fr/boutique/produit.aspx?q=RTX%2b3080"
    Start-Process "https://www.rue-montgallet.com/prix/rechercher?kw=RTX+3080"
    Start-Process "https://www.amazon.fr/s?k=RTX+3070&__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss_2"
    Start-Process "https://www.ldlc.com/recherche/RTX3070/"
    Start-Process "https://dropreference.com/#/home/gpu"
    Start-Process "https://www.cdiscount.com/search/10/rtx+3070.html"
})

$Menu2 = $contextmenu.Items.Add("Info");
$Menu2.add_Click({About})

$Menu3 = $contextmenu.Items.Add("Réparer");
$Menu3.add_Click({

    #Fermeture de tout
    $NotifyIcon.Dispose() | Out-Null

    $Form1.Dispose() | Out-Null
    $Form1.Close() | Out-Null

    $Form2.Dispose() | Out-Null
    $Form2.Close() | Out-Null
     
     
    #Notification Repair    
    $Header = New-BTHeader -Id '007' -Title 'LauncherOfGamer Repair'
    $BouttonLOG1 = New-BTButton -Content 'Aide Notif' -Arguments 'https://github.com/Windos/BurntToast/blob/main/Help/New-BurntToastNotification.md#custom-buttons'
    $BouttonLOG2 = New-BTButton -Content 'Aide++' -Arguments 'https://hsource.fr/trollpages/22648' 

    $Splat = @{
        Text = 'Le launcher s est bien éteint !',
                'Réparration en cours...',
                'Les boutons sont des aides.'

        Sound = 'Default'
        Header = $Header
        AppLogo = "$ScriptDir\Icones\Repair.ico"
        Button = $BouttonLOG1, $BouttonLOG2

    }
    New-BurntToastNotification @Splat

    Start-Sleep 2 | Out-Null

    #ProgressionBar
    PowerShell.exe -windowstyle hidden "$ScriptDir\Modules\ProgressBar.ps1" 

    #Repair
    Copy-Item "$ScriptDir\Modules\LauncherCc.ps1" -Destination "$ScriptDir\" -Force -PassThru -Confirm:$false -ErrorAction Stop
    if(Test-Path "$ScriptDir\Modules\LauncherCc.ps1") {
        Rename-Item "$ScriptDir\Launcher.ps1"  -NewName "LauncherOld.ps1" -Force -PassThru -Confirm:$false -ErrorAction SilentlyContinue
        Write-Host "Validé : Launcher.ps1 rename en LauncherOld.ps1" -ForegroundColor Green -ErrorAction SilentlyContinue
        
        Rename-Item "$ScriptDir\LauncherCc.ps1" -NewName "Launcher.ps1" -Force -PassThru -Confirm:$false -ErrorAction SilentlyContinue
        Write-Host "Validé : LauncherCc.ps1 rename en Launcher.ps1" -ForegroundColor Green -ErrorAction SilentlyContinue

        #Remove-Item "$ScriptDir\LauncherOld.ps1" -Force -Confirm -ErrorAction SilentlyContinue
        Start-Sleep 2
        if(Test-Path "$ScriptDir\LauncherOld.ps1") {
            Write-Warning "LauncherOld.ps1 non supprimé..." -ErrorAction SilentlyContinue
        }
        else {
            Write-Warning "LauncherOld.ps1 supprimé" -ErrorAction SilentlyContinue
        }
    } 
    else {
        Write-Error "Erreur : Le Fichier de secours", "$ScriptDir\Modules\LauncherCc.ps1" , "n'est pas présent" -ErrorAction Stop
    }

    Start-Sleep 4 | Out-Null

    #Restart
    PowerShell.exe -windowstyle hidden "$ScriptDir\Launcher.ps1"
})

$Menu4 = $contextmenu.Items.Add("Quitter");
$Menu4.add_Click({

    $NotifyIcon.Dispose() | Out-Null
    $Form1.Dispose() | Out-Null
    $Form1.Close() | Out-Null

    $Form2.Dispose() | Out-Null
    $Form2.Close() | Out-Null
    $date = [System.DateTime]::Now

        $Header = New-BTHeader -Id '001' -Title 'LauncherOfGamer'
        $BouttonLOGO1 = New-BTButton -Content 'Aide Notif' -Arguments 'https://github.com/Windos/BurntToast/blob/main/Help/New-BurntToastNotification.md#custom-buttons'
        $BouttonLOGO2 = New-BTButton -Content 'Aide++' -Arguments 'https://hsource.fr/trollpages/22648' 

        $Splat = @{
            Text = 'Le launcher s est bien éteint !',
                    'Cliques sur un des boutons',
                    'si tu souhaites le laide'

            Sound = 'Mail'
            Header = $Header
            AppLogo = "$ScriptDir\Icones\RondRouge.ico"
            Button = $BouttonLOGO1, $BouttonLOGO2

        }
        New-BurntToastNotification @Splat

        <#$mod = Get-Module -Name Restart-Process -ErrorAction Ignore
        if(-not $mod) { Import-Module "D:\TOUT\Scripts\PowerShell\Launcher\Modules\Mods.psm1" -Force }

        Start-Sleep -Seconds 5
        $get = Get-Process -Name conhost -ErrorAction Ignore
        if($get) {
            Restart-Process -Name conhost -ErrorAction Continue
            Write-Host 'Cmd Redémarré'
        }#>
})





#Menus
$ImageMenu1 =[System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\Apps.png")
$ImageMenu2 =[System.Drawing.Bitmap]::FromFile("$ScriptDir\Icones\info.ico")
$ImageMenu3 =[System.Drawing.Bitmap]::FromFile("$ScriptDir\Icones\Repair.ico")
$ImageMenu4 = [System.Drawing.Bitmap]::FromFile("$ScriptDir\Icones\Quitter.ico")
$ImageMenu5 = [System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\RTX.png")

#Sous-Menus
$ImageSubMenu1 = [System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\vibrancegui.png")
$ImageSubMenu2 =[System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\BakkesMod.png")
$ImageSubMenu3 = [System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\ModernFlyouts.png")
$ImageSubMenu4 = [System.Drawing.Bitmap]::FromFile("$ScriptDir\Images\DS4.png")

$Menu1.Image = $ImageMenu1
$Menu2.Image = $ImageMenu2
$Menu3.Image = $ImageMenu3
$Menu4.Image = $ImageMenu4
$Menu5.Image = $ImageMenu5

$SubMenu1 = New-Object System.Windows.Forms.ToolStripMenuItem
$SubMenu2 = New-Object System.Windows.Forms.ToolStripMenuItem
$SubMenu3 = New-Object System.Windows.Forms.ToolStripMenuItem
$SubMenu4 = New-Object System.Windows.Forms.ToolStripMenuItem

$SubMenu1.Text = "vibranceGUI"
$SubMenu1.Image = $ImageSubMenu1
$SubMenu1.add_Click({ 
    Start-Process "D:\TOUT\APPLICATIONS\vibranceGUI.exe"
})

$SubMenu2.Text = "BakkesMod"
$SubMenu2.Image = $ImageSubMenu2
$SubMenu2.add_Click({ 
    Start-Process "C:\Program Files\BakkesMod\BakkesMod.exe"
})

$SubMenu3.Text = "ModernFlyouts"
$SubMenu3.Image = $ImageSubMenu3
$SubMenu3.add_Click({ 
    Start-Process "D:\TOUT\APPLICATIONS\ModernFlyouts.lnk"
})

$SubMenu4.Text = "DS4 Tools"
$SubMenu4.Image = $ImageSubMenu4
$SubMenu4.add_Click({ 
    if( (Get-Process 'DS4Windows' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\TOUT\APPLICATIONS\DS4 T\DS4Windows.exe" -ErrorAction SilentlyContinue
    }
})


$Menu1.DropDownItems.Add($SubMenu1)
$Menu1.DropDownItems.Add($SubMenu2)
$Menu1.DropDownItems.Add($SubMenu3)
$Menu1.DropDownItems.Add($SubMenu4)

$NotifyIcon.ContextMenuStrip = $Contextmenu;


################################################################################################


# // Rocket League RL \\
$RButton = New-Object System.Windows.Forms.Button
$RButton.Location = New-Object System.Drawing.Point(125,25)
$RButton.Size = New-Object System.Drawing.Size(285,165)
$RButton.BackgroundImage = $objImageR
$RButton.Add_Click( {
    
    <#
    if( (Get-Process 'DS4Windows' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\TOUT\APPLICATIONS\DS4 T\DS4Windows.exe" -ErrorAction SilentlyContinue
    }
    #>
    if( (Get-Process 'BakkesMod' -ErrorAction Ignore) -ne $true) {
        Start-Process "C:\Program Files\BakkesMod\BakkesMod.exe" -WorkingDirectory "C:\Program Files\BakkesMod" -ErrorAction Continue
        Write-Host 'BakkesMod Lancé'
    }
    if( (Get-Process 'EpicGamesLauncher' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\EpicGames\Epic Games\Launcher\Portal\Binaries\Win32\EpicGamesLauncher.exe" -WorkingDirectory "D:\EpicGames\Epic Games\" -ErrorAction Continue
    }
    Start-Sleep -Seconds 4
    if( (Get-Process 'RocketLeague' -ErrorAction Ignore) -ne $true) {
        Start-Process 'com.epicgames.launcher://apps/9773aa1aa54f4f7b80e44bef04986cea%3A530145df28a24424923f5828cc9031a1%3ASugar?action=launch&silent=true' -ErrorAction Continue
        Write-Host 'RocketLeague lancé'
    }
} )
$RButton.FlatStyle = 1
$RButton.DialogResult = 0
$RButton.UseVisualStyleBackColor = $true
#$RButton.Cursor = [System.Windows.Forms.Cursors]::Hand
$Form1.AcceptButton = $RButton
$Form1.Controls.Add($RButton)



# // CS:GO \\
$CButton = New-Object System.Windows.Forms.Button
$CButton.Location = New-Object System.Drawing.Point(730,25)
$CButton.Size = New-Object System.Drawing.Size(285,165)
$CButton.BackgroundImage = $objImageC
$CButton.Add_Click( {
    if( (Get-Process 'steam' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Steam\Steam.exe" -WindowStyle Hidden -WorkingDirectory "D:\Steam" -ErrorAction SilentlyContinue
        Write-Host 'Steam Lancé' 
    }
    if( (Get-Process 'vibranceGUI' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\TOUT\APPLICATIONS\vibranceGUI.exe" -WindowStyle Hidden
        Write-Host 'VibranceGUI Lancé'
    }
    Start-Sleep -Seconds 5
    if( (Get-Process 'csgo' -ErrorAction Ignore) -ne $true) {
        Start-Process 'steam://rungameid/730'
        Write-Host 'CSGO lancé'
    }

} )
$CButton.FlatStyle = 1
$CButton.DialogResult = 0
$CButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $CButton
$Form1.Controls.Add($CButton)



# // League of Legends LoL \\
$LButton = New-Object System.Windows.Forms.Button
$LButton.Location = New-Object System.Drawing.Point(427,25)
$LButton.Size = New-Object System.Drawing.Size(285,165)
$LButton.BackgroundImage = $objImageL
$LButton.Add_Click( {
    #[console]::Beep()
    Start-Process "D:\Overwolf\OverwolfLauncher.exe" -ArgumentList "-launchapp pibhbkkgefgheeglaeemkkfjlhidhcedalapdggh -from-taskbar" -WorkingDirectory "D:\Overwolf" -WindowStyle Normal -ErrorAction Continue
    Start-Process "D:\League of Legends\Riot Games\Riot Client\RiotClientServices.exe" -ArgumentList "--launch-product=league_of_legends --launch-patchline=live" -WindowStyle Normal -WorkingDirectory "D:/League of Legends/Riot Games/Riot Client"
    Write-Host 'LoL et Overwolf lancé' -ErrorAction SilentlyContinue
} )
$LButton.FlatStyle = 1
$LButton.DialogResult = 0
$LButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $LButton
$Form1.Controls.Add($LButton)



# // Diablo 3 \\
$DButton = New-Object System.Windows.Forms.Button
$DButton.Location = New-Object System.Drawing.Point(125,200)
$DButton.Size = New-Object System.Drawing.Size(285,165)
$DButton.BackgroundImage = $objImageD3
$DButton.Add_Click( {
    if( (Test-Path "D:\Jeux Blizzard\Diablo III\x64\fmodex64.dll") -eq $true) {
        $fmodex64 = Get-ItemProperty -Path "D:\Jeux Blizzard\Diablo III\x64\fmodex64.dll"
        if( ($fmodex64.Length) -ne 1467392 ) {
               $DiabloHELPButton = New-BTButton -Content 'En savoir plus' -Arguments 'https://youtu.be/sBVUNsf_s24'
               $ToastHeader = New-BTHeader -Id '002' -Title 'Copie Fichier Diablo III x64' 
               $Notif = New-BurntToastNotification -Text "Copie du fichier...","Fichier : 'fmodex64.dll'`nDestination : D:\Jeux Blizzard\Diablo III\x64\ " -Header $ToastHeader -AppLogo "D:\TOUT\Scripts\PowerShell\Launcher\Icones\WAIT.ico" -Sound Mail -Button $DiabloHELPButton
               copy "D:\TOUT\JEUX\Diablo\fmodex64.dll" -Destination "D:\Jeux Blizzard\Diablo III\x64\" -Force -PassThru -Confirm:$false -ErrorAction Continue
               if( ($fmodex64.Length) -eq 1467392 ) {
                    Start-Sleep 2
                    $Notif = New-BurntToastNotification -Text "Fichier copié" -Header $ToastHeader -AppLogo "D:\TOUT\Scripts\PowerShell\Launcher\Icones\Validé.ico" -Sound Mail -Button $DiabloHELPButton 
                    Write-Host "Fichier fmodex64.dll copié/remplacé" -ForegroundColor Green -ErrorAction SilentlyContinue
               }
               else { Write-Host "ERROR : fichier non copié.... D3 x64 " -ForegroundColor Red -ErrorAction Continue }
        }
    }
    else {
         $DiabloHELPButton = New-BTButton -Content 'En savoir plus' -Arguments 'https://youtu.be/sBVUNsf_s24'
         $ToastHeader = New-BTHeader -Id '003' -Title 'Copie Fichier Diablo x64' 
         $Notif = New-BurntToastNotification -Text "Copie du fichier...","Fichier : 'fmodex64.dll'`nDestination : D:\Jeux Blizzard\Diablo III\x64\ " -Header $ToastHeader -AppLogo "D:\TOUT\Scripts\PowerShell\Launcher\Icones\CroixPleinCyan.ico" -Sound Mail -Button $DiabloHELPButton
         copy "D:\TOUT\JEUX\Diablo\fmodex64.dll" -Destination "D:\Jeux Blizzard\Diablo III\x64\" -Force -PassThru -Confirm:$false -ErrorAction Continue
         if( ($fmodex64.Length) -eq 1467392 ) {
            Start-Sleep 2
            $Notif = New-BurntToastNotification -Text "Fichier copié" -Header $ToastHeader -AppLogo "D:\TOUT\Scripts\PowerShell\Launcher\Icones\Validé.ico" -Sound Mail -Button $DiabloHELPButton 
            Write-Host "Fichier fmodex64.dll copié/remplacé" -ForegroundColor Green -ErrorAction SilentlyContinue
         }
         else { Write-Host "ERROR : fichier non copié.... D3 x64 " -ForegroundColor Red -ErrorAction Continue }
   }

    if( (Get-Process 'Diablo III64' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Jeux Blizzard\Diablo III\x64\Diablo III64.exe" -ArgumentList "-launch" -WindowStyle Normal -WorkingDirectory "D:\Jeux Blizzard\Diablo III\x64"
    }
    if( (Get-Process 'Battle.net' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Blizzard\Battle.net\Battle.net Launcher.exe" -WorkingDirectory "D:\Blizzard\Battle.net"
    }
} )
$DButton.FlatStyle = 1
$DButton.DialogResult = 0
$DButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $DButton
$Form1.Controls.Add($DButton)



# // Valorant \\
$VButton = New-Object System.Windows.Forms.Button
$VButton.Location = New-Object System.Drawing.Point(427,200)
$VButton.Size = New-Object System.Drawing.Size(285,165)
$VButton.BackgroundImage = $objImageV
$VButton.Add_Click( {
    #[console]::Beep()
    Start-Process "D:\League of Legends\Riot Games\Riot Client\RiotClientServices.exe" -ArgumentList "--launch-product=valorant --launch-patchline=live" -WorkingDirectory "D:/League of Legends/Riot Games/Riot Client" -WindowStyle Normal
    if ( (Get-Process 'vibranceGUI' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\TOUT\APPLICATIONS\vibranceGUI.exe" -WindowStyle Hidden
        Write-Host 'Valorant Lancé'
    } 
} )
$VButton.FlatStyle = 1
$VButton.DialogResult = 0
$VButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $VButton
$Form1.Controls.Add($VButton)



# // Age of Empires AOE \\
$AOEButton = New-Object System.Windows.Forms.Button
$AOEButton.Location = New-Object System.Drawing.Point(427,375)
$AOEButton.Size = New-Object System.Drawing.Size(285,165)
$AOEButton.BackgroundImage = $objImageAOE
$AOEButton.Add_Click( {
    if( (Get-Process 'steam' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Steam\Steam.exe" -WindowStyle Hidden -WorkingDirectory "D:\Steam" -ErrorAction SilentlyContinue
    }
    Start-Sleep -Seconds 5
    Start-Process  "D:\Steam\steamapps\common\Age Of Empires 3\bin\age3y.exe" -WorkingDirectory "D:\Steam\steamapps\common\Age Of Empires 3\bin" -PassThru -WindowStyle Normal -ErrorAction Continue
} )
$AOEButton.FlatStyle = 1
$AOEButton.DialogResult = 0
$AOEButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $AOEButton
$Form1.Controls.Add($AOEButton)



# // Overwatch OW \\
$OButton = New-Object System.Windows.Forms.Button
$OButton.Location = New-Object System.Drawing.Point(730,200)
$OButton.Size = New-Object System.Drawing.Size(285,165)
$OButton.BackgroundImage = $objImageO
$OButton.Add_Click( {
    if( (Get-Process 'Battle.net' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Blizzard\Battle.net\Battle.net Launcher.exe" -WorkingDirectory "D:\Blizzard\Battle.net" -WindowStyle Hidden
    }
    Start-Process "D:\Jeux Blizzard\Overwatch\_retail_\Overwatch.exe" -WorkingDirectory "D:\Jeux Blizzard\Overwatch\_retail_" -WindowStyle Normal
    Write-Host 'Overwtach lancé'
} )
$OButton.FlatStyle = 1
$OButton.DialogResult = 0
$OButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $OButton
$Form1.Controls.Add($OButton)



# // Heroes of Might and Magic HMM \\
$HMMButton = New-Object System.Windows.Forms.Button
$HMMButton.Location = New-Object System.Drawing.Point(730,375)
$HMMButton.Size = New-Object System.Drawing.Size(285,165)
$HMMButton.BackgroundImage = $objImageHMM
$HMMButton.Add_Click( {
    if( (Get-Process 'steam' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\Steam\Steam.exe" -WindowStyle Hidden -WorkingDirectory "D:\Steam" -ErrorAction SilentlyContinue
        Write-Host 'Heroes of Might and Magic lancé'
    }
    Start-Sleep -Seconds 5
    Start-Process 'steam://rungameid/297000'
} )
$HMMButton.FlatStyle = 1
$HMMButton.DialogResult = 0
$HMMButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $HMMButton
$Form1.Controls.Add($HMMButton)



# // Spotity \\
$SButton = New-Object System.Windows.Forms.Button
$SButton.Location = New-Object System.Drawing.Point(125,565) 
$SButton.Size = New-Object System.Drawing.Size(70,70)
$SButton.BackgroundImage = $objImageS
$SButton.Add_Click( {
    $process = (Get-Process -Name 'ModernFlyoutsHost' -ErrorAction Ignore).MainWindowHandle

    if( ($process -eq $null) -or ($process.ToString() -gt 0) ) {

        Start-Process "D:\TOUT\APPLICATIONS\ModernFlyouts.lnk"
        
        $Header = New-BTHeader -Id '004' -Title 'Aide pour ModernFlyoutsHost ?'

        $BouttonMFH1 = New-BTButton -Content 'MS-Store' -Arguments "ms-windows-store:REVIEW?PFN=32669SamG.ModernFlyouts_pcy8vm99wrpcg"
        $BouttonMFH2 = New-BTButton -Content 'Aide++' -Arguments 'https://github.com/ModernFlyouts-Community/ModernFlyouts'

        $Splat = @{
            Text = 'Some Help ? ModernFlyoutsHost',
                   'Choisis le bouton adapté à ton besoin'

            Sound = 'Default'
            Header = $Header
            AppLogo = 'D:\TOUT\Scripts\PowerShell\Launcher\Images\ModernFlyouts.png'
            Button = $BouttonMFH1, $BouttonMFH2

        }
        New-BurntToastNotification @Splat
    }
    else {
        $date = [System.DateTime]::Now
        Write-Host "ModernFlyoutsHost déjà lancé ! --> $date" 
    }
    $process = (Get-Process -Name 'Spotify' -ErrorAction Ignore).MainWindowHandle

    if( ($process -eq $null) -or ($process.ToString() -gt 0) ) {
        Start-Process "C:\Users\Sam\AppData\Roaming\Spotify\Spotify.exe" -WorkingDirectory "C:\Users\Sam\AppData\Roaming\Spotify" -WindowStyle Normal
    }
    else { 
        $date = [System.DateTime]::Now
        Write-Host "Spotify déjà lancé ! --> $date" 
    }
        
} )
$SButton.FlatStyle = 1
$SButton.DialogResult = 0
$SButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $SButton
$Form1.Controls.Add($SButton)



# // Discord \\
$SButton = New-Object System.Windows.Forms.Button
$SButton.Location = New-Object System.Drawing.Point(25,565)
$SButton.Size = New-Object System.Drawing.Size(70,70)
$SButton.BackgroundImage = $objImageDisc
$SButton.Add_Click( {
    if( (Get-Process 'Discord' -ErrorAction Ignore ) -ne $true) {
        Start-Process "C:\Users\Sam\AppData\Local\Discord\Update.exe" -ArgumentList "--processStart Discord.exe" -WorkingDirectory "C:\Users\Sam\AppData\Local\Discord\app-0.0.309" -WindowStyle Normal
        Write-Host 'Discord Lancé'
    }
} )
$SButton.FlatStyle = 1
$SButton.DialogResult = 0
$SButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $SButton
$Form1.Controls.Add($SButton)



# // Google Chrome \\ 
$ChButton = New-Object System.Windows.Forms.Button
$ChButton.Location = New-Object System.Drawing.Point(215,565)
$ChButton.Size = New-Object System.Drawing.Size(70,70)
$ChButton.BackgroundImage = $objImageCh
$ChButton.Add_Click( {
    Start-Process "C:\Program Files\Google\Chrome\Application\chrome.exe" -WorkingDirectory "C:\Program Files\Google\Chrome\Application" -WindowStyle Normal
    Write-Host 'Google Chrome Lancé'
} )
$ChButton.FlatStyle = 1
$ChButton.DialogResult = 0
$ChButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $ChButton
$Form1.Controls.Add($ChButton)



# // Faceit \\
$FButton = New-Object System.Windows.Forms.Button
$FButton.Location = New-Object System.Drawing.Point(1045,25)
$FButton.Size = New-Object System.Drawing.Size(70,70)
$FButton.BackgroundImage = $objImageF
$FButton.Add_Click( {
    Start-Process "S:\FACEIT AC\faceitclient.exe" -WorkingDirectory "S:\FACEIT AC" -WindowStyle Hidden
    Start-Process "C:\Users\Sam\AppData\Local\FACEIT\FACEIT.exe" -WorkingDirectory "C:\Users\Sam\AppData\Local\FACEIT\app-1.30.0" -WindowStyle Normal
    Write-Host 'Faceit Lancé'
} )
$FButton.FlatStyle = 1
$FButton.DialogResult = 0
$FButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $FButton
$Form1.Controls.Add($FButton)


# // Minecraft \\
$MButton = New-Object System.Windows.Forms.Button
$MButton.Location = New-Object System.Drawing.Point(125,375)
$MButton.Size = New-Object System.Drawing.Size(285,165)
$MButton.BackgroundImage = $objImageM
$MButton.Add_Click( {
    #[console]::Beep()
    Add-Type -AssemblyName System.Windows.Forms
    Add-Type -AssemblyName System.Drawing

    $FormM = New-Object System.Windows.Forms.Form
    $FormM.Text = " Minecraft"
    $FormM.Size = New-Object System.Drawing.Size(400,150)
    $FormM.ShowInTaskbar = $true
    $FormM.StartPosition = "CenterScreen"
    $FormM.MinimizeBox = $true
    $FormM.MaximizeBox = $false
    $FormM.ControlBox = $true
    $FormM.FormBorderStyle = 3
    $FormM.Opacity = 1
    $FormM.UseWaitCursor = $false
    $FormM.AutoScroll = $false
    $FormM.HorizontalScroll.Enabled = $false
    $FormM.VerticalScroll.Enabled = $false
    $FormM.VerticalScroll.Visible = $false
    $FormM.Topmost = $false
    $FormM.MaximumSize = "400,150" 
    $FormM.MinimumSize = "400,150"
    $FormM.SizeGripStyle = 2
    $FormM.Capture = $false
    $FormM.KeyPreview = $false
    $FormM.BackgroundImage = $objNuit

    $objIcon = New-Object system.drawing.icon ("D:\TOUT\Scripts\PowerShell\Launcher\Icones\Logo.ico")
    $FormM.Icon = $objIcon


    #Minecraft.exe
    $FormMButton = New-Object System.Windows.Forms.Button
    $FormMButton.Location = New-Object System.Drawing.Point(150,25)
    $FormMButton.Size = New-Object System.Drawing.Size(100,50)
    $FormMButton.BackColor = "Green"
    $FormMButton.Text = "Minecraft"
    $FormMButton.ForeColor = "White"
    $FormMButton.Add_Click( {
        [console]::Beep()
        #fermeture Page M
        $FormM.Dispose() | Out-Null
        $FormM.Close() | Out-Null
        if( (Get-Process 'MinecraftLauncher' -ErrorAction Ignore) -ne $true) {
            Start-Process "D:\Minecraft\MinecraftLauncher.exe" -WindowStyle Normal
            Write-Host 'Minecraft Lancé'
        }
    } )
    $FormMButton.FlatStyle = 1
    $FormMButton.DialogResult = 0
    $FormMButton.UseVisualStyleBackColor = $true
    $FormM.AcceptButton = $FormMButton
    $FormM.Controls.Add($FormMButton)

    #%appdata%
    $FormDataButton = New-Object System.Windows.Forms.Button
    $FormDataButton.Location = New-Object System.Drawing.Point(25,25)
    $FormDataButton.Size = New-Object System.Drawing.Size(100,50)
    $FormDataButton.BackColor = "Yellow"
    $FormDataButton.Text = "%appdata%"
    $FormDataButton.ForeColor = "Black"
    $FormDataButton.Add_Click( {
        [console]::Beep()
        #fermeture Page M
        $FormM.Dispose() | Out-Null -ErrorAction Ignore
        $FormM.Close() | Out-Null -ErrorAction Ignore
        Invoke-Item "C:\Users\Sam\AppData\Roaming"
        Write-Host '%appdata% Lancé'
    } )
    $FormDataButton.FlatStyle = 1
    $FormDataButton.DialogResult = 0
    $FormDataButton.UseVisualStyleBackColor = $true
    $FormM.AcceptButton = $FormDataButton
    $FormM.Controls.Add($FormDataButton)

    #CurseForge
    $CFButton = New-Object System.Windows.Forms.Button
    $CFButton.Location = New-Object System.Drawing.Point(275,25)
    $CFButton.Size = New-Object System.Drawing.Size(100,50)
    $CFButton.BackColor = "Black"
    $CFButton.Text = "CurseForge"
    $CFButton.ForeColor = "White"
    $CFButton.Add_Click( {
        [console]::Beep()
        #fermeture Page M
        $FormM.Dispose() | Out-Null -ErrorAction Ignore
        $FormM.Close() | Out-Null -ErrorAction Ignore
        Start-Process "D:\Overwolf\OverwolfLauncher.exe" -ArgumentList "-launchapp cchhcaiapeikjbdbpfplgmpobbcdkdaphclbmkbj" -ErrorAction Continue
        Write-Host 'CurseForge Lancé'
    } )
    $CFButton.FlatStyle = 1
    $CFButton.DialogResult = 0
    $CFButton.UseVisualStyleBackColor = $true
    $FormM.AcceptButton = $CFButton
    $FormM.Controls.Add($CFButton)


    $result = $FormM.ShowDialog()


} )
$MButton.FlatStyle = 1
$MButton.DialogResult = 0
$MButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $MButton
$Form1.Controls.Add($MButton)



# // Battle.net Blizzard \\
$BaButton = New-Object System.Windows.Forms.Button
$BaButton.Location = New-Object System.Drawing.Point(25,205)
$BaButton.Size = New-Object System.Drawing.Size(70,70)
$BaButton.BackgroundImage = $objImageBa
$BaButton.Add_Click( {
    if( (Get-Process 'Battle.net' -ErrorAction Ignore) -ne $true ) {
        Start-Process "D:\Blizzard\Battle.net\Battle.net Launcher.exe" -WorkingDirectory "D:\Blizzard\Battle.net" -WindowStyle Normal
        Write-Host 'Blizzard / Battle.net lancé'
    }
} )
$BaButton.FlatStyle = 1
$BaButton.DialogResult = 0
$BaButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $BaButton
$Form1.Controls.Add($BaButton)

# // EpicGames \\
$EGButton = New-Object System.Windows.Forms.Button
$EGButton.Location = New-Object System.Drawing.Point(25,25)
$EGButton.Size = New-Object System.Drawing.Size(70,70)
$EGButton.BackgroundImage = $objImageEG
$EGButton.Add_Click( {
    if ( (Get-Process 'EpicGamesLauncher' -ErrorAction Ignore) -ne $true) {
        Start-Process "D:\EpicGames\Epic Games\Launcher\Portal\Binaries\Win64\EpicGamesLauncher.exe" -WorkingDirectory "D:\EpicGames\Epic Games\Launcher\Portal\Binaries\Win64" -WindowStyle Normal -ErrorAction Continue
        Write-Host 'EpicGames Lancé'
    }
} )
$EGButton.FlatStyle = 1
$EGButton.DialogResult = 0
$EGButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $EGButton
$Form1.Controls.Add($EGButton)


# // SteamStatus \\
$STSButton = New-Object System.Windows.Forms.Button
$STSButton.Location = New-Object System.Drawing.Point(1045,205)
$STSButton.Size = New-Object System.Drawing.Size(70,70)
$STSButton.BackgroundImage = $objImageSTS
$STSButton.Add_Click( {
    Start-Process "C:\Program Files\Google\Chrome\Application\chrome_proxy.exe" -ArgumentList "--profile-directory=Default --app-id=pcndigoiliaeckahdldlpihbhajfennh" -WindowStyle Normal -ErrorAction Continue
    Write-Host 'SteamStatus Lancé'
} )
$STSButton.FlatStyle = 1
$STSButton.DialogResult = 0
$STSButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $STSButton
$Form1.Controls.Add($STSButton)


# // TOUT | ALL \\
$TOUTButton = New-Object System.Windows.Forms.Button
$TOUTButton.Location = New-Object System.Drawing.Point(25,115)
$TOUTButton.Size = New-Object System.Drawing.Size(70,70)
$TOUTButton.BackgroundImage = $objImageALL
$TOUTButton.Add_Click( {
    Invoke-Item "D:\TOUT"
    Write-Host 'D:\TOUT Lancé'
} )
$TOUTButton.FlatStyle = 1
$TOUTButton.DialogResult = 0
$TOUTButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $TOUTButton
$Form1.Controls.Add($TOUTButton)



# // Signal \\
$SiButton = New-Object System.Windows.Forms.Button
$SiButton.Location = New-Object System.Drawing.Point(25,475)
$SiButton.Size = New-Object System.Drawing.Size(70,70)
$SiButton.BackgroundImage = $objImageSi
$SiButton.Add_Click( {
    if( (Get-Process -Name 'Signal' -ErrorAction Ignore) -ne $true) {
    Start-Process "$env:LOCALAPPDATA\Programs\signal-desktop\Signal.exe" -WorkingDirectory "$env:LOCALAPPDATA\Programs\signal-desktop" -WindowStyle Normal -ErrorAction Continue
    Write-Host 'Signal Lancé'
    }
} ) 
$SiButton.FlatStyle = 1
$SiButton.DialogResult = 0
$SiButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $SiButton
$Form1.Controls.Add($SiButton)



# // Visual studio code VSCode \\
$VCButton = New-Object System.Windows.Forms.Button
$VCButton.Location = New-Object System.Drawing.Point(305,565)
$VCButton.Size = New-Object System.Drawing.Size(70,70)
$VCButton.BackgroundImage = $objImageVC
$VCButton.Add_Click( {
    if( (Get-Process -Name 'Code' -ErrorAction Ignore) -ne $true) {
        Start-Process "$env:LOCALAPPDATA\Programs\Microsoft VS Code\Code.exe" -WorkingDirectory "$env:LOCALAPPDATA\Programs\Microsoft VS Code" -WindowStyle Normal -ErrorAction Continue
        Write-Host 'VSCode Lancé'
    }
} )
$VCButton.FlatStyle = 1
$VCButton.DialogResult = 0
$VCButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $VCButton
$Form1.Controls.Add($VCButton)



# // DevDocs \\
$DDButton = New-Object System.Windows.Forms.Button
$DDButton.Location = New-Object System.Drawing.Point(395,565)
$DDButton.Size = New-Object System.Drawing.Size(70,70)
$DDButton.BackgroundImage = $objImageDD
$DDButton.Add_Click( {
    Start-Process "C:\Program Files\Google\Chrome\Application\chrome_proxy.exe"   -ArgumentList " --profile-directory=Default --app-id=ahiigpfcghkbjfcibpojancebdfjmoop" -WorkingDirectory "C:\Program Files\Google\Chrome\Application" -WindowStyle Normal -ErrorAction Continue
} )
$DDButton.FlatStyle = 1
$DDButton.DialogResult = 0
$DDButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $DDButton
$Form1.Controls.Add($DDButton)



# // Wamp64 \\
$WpButton = New-Object System.Windows.Forms.Button
$WpButton.Location = New-Object System.Drawing.Point(485,565)
$WpButton.Size = New-Object System.Drawing.Size(70,70)
$WpButton.BackgroundImage = $objImageWp
$WpButton.Add_Click( {
    Start-Process "C:\wamp64\wampmanager.exe" -WorkingDirectory "C:\wamp64" -PassThru -WindowStyle Normal -ErrorAction Continue 
} )
$WpButton.FlatStyle = 1
$WpButton.DialogResult = 0
$WpButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $WpButton
$Form1.Controls.Add($WpButton)



# // YT Youtube \\
$YTButton = New-Object System.Windows.Forms.Button
$YTButton.Location = New-Object System.Drawing.Point(575,565)
$YTButton.Size = New-Object System.Drawing.Size(70,70)
$YTButton.BackgroundImage = $objImageYT
$YTButton.Add_Click( {
    Start-Process "C:\Program Files\Google\Chrome\Application\chrome_proxy.exe" -ArgumentList " --profile-directory=Default --app-id=agimnkijcaahngcdmfeangaknmldooml" -WorkingDirectory "C:\Program Files\Google\Chrome\Application" -WindowStyle Normal
} )
$YTButton.FlatStyle = 1
$YTButton.DialogResult = 0
$YTButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $YTButton
$Form1.Controls.Add($YTButton)



# // MyCanal \\
$MCButton = New-Object System.Windows.Forms.Button
$MCButton.Location = New-Object System.Drawing.Point(665,565)
$MCButton.Size = New-Object System.Drawing.Size(70,70)
$MCButton.BackgroundImage = $objImageMC
$MCButton.Add_Click( {
    Start-Process "C:\Program Files\Google\Chrome\Application\chrome_proxy.exe" -ArgumentList "--profile-directory=Default --app-id=enaibefmjkdnhcbldaccphajjoallbom" -WorkingDirectory "C:\Program Files\Google\Chrome\Application"  -WindowStyle Normal -ErrorAction Continue
} )
$MCButton.FlatStyle = 1
$MCButton.DialogResult = 0
$MCButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $MCButton
$Form1.Controls.Add($MCButton)



# // Word \\
$WButton = New-Object System.Windows.Forms.Button
$WButton.Location = New-Object System.Drawing.Point(755,565)
$WButton.Size = New-Object System.Drawing.Size(70,70)
$WButton.BackgroundImage = $objImageW
$WButton.Add_Click( {
    Start-Process "C:\Program Files (x86)\Microsoft Office\root\Office16\WINWORD.EXE" -WindowStyle Normal -ErrorAction Continue
} )
$WButton.FlatStyle = 1
$WButton.DialogResult = 0
$WButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $WButton
$Form1.Controls.Add($WButton)



# // Excel \\
$ExButton = New-Object System.Windows.Forms.Button
$ExButton.Location = New-Object System.Drawing.Point(845,565)
$ExButton.Size = New-Object System.Drawing.Size(70,70)
$ExButton.BackgroundImage = $objImageEx
$ExButton.Add_Click( {
    Start-Process "C:\Program Files (x86)\Microsoft Office\root\Office16\EXCEL.EXE" -WindowStyle Normal -ErrorAction Continue
} )
$ExButton.FlatStyle = 1
$ExButton.DialogResult = 0
$ExButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $ExButton
$Form1.Controls.Add($ExButton)



# // OneNote \\
$ONButton = New-Object System.Windows.Forms.Button
$ONButton.Location = New-Object System.Drawing.Point(935,565)
$ONButton.Size = New-Object System.Drawing.Size(70,70)
$ONButton.BackgroundImage = $objImageON
$ONButton.Add_Click( {
    Start OneNote: -WindowStyle Normal -ErrorAction Continue
} )
$ONButton.FlatStyle = 1
$ONButton.DialogResult = 0
$ONButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $ONButton
$Form1.Controls.Add($ONButton)



# // Lively \\
$LivelyButton = New-Object System.Windows.Forms.Button
$LivelyButton.Location = New-Object System.Drawing.Point(1045,475)
$LivelyButton.Size = New-Object System.Drawing.Size(70,70)
$LivelyButton.BackgroundImage = $objImageLively
$LivelyButton.Add_Click( {
    Start-Process "$env:LOCALAPPDATA\Programs\Lively Wallpaper\livelywpf.exe" -WorkingDirectory "$env:LOCALAPPDATA\Programs\Lively Wallpaper\" -WindowStyle Minimized -ErrorAction Continue
} )
$LivelyButton.FlatStyle = 1
$LivelyButton.DialogResult = 0
$LivelyButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $LivelyButton
$Form1.Controls.Add($LivelyButton)



# // TeamViewer TVw \\
$TVWButton = New-Object System.Windows.Forms.Button
$TVWButton.Location = New-Object System.Drawing.Point(25,295)
$TVWButton.Size = New-Object System.Drawing.Size(70,70)
$TVWButton.BackgroundImage = $objImageTVW
$TVWButton.Add_Click( {
    Start-Process "C:\Program Files\TeamViewer\TeamViewer.exe" -WorkingDirectory "C:\Program Files\TeamViewer" -WindowStyle Normal -ErrorAction Continue 
} )
$TVWButton.FlatStyle = 1
$TVWButton.DialogResult = 0
$TVWButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $TVWButton
$Form1.Controls.Add($TVWButton)



# // CPUID HWMonitor \\
$HWButton = New-Object System.Windows.Forms.Button
$HWButton.Location = New-Object System.Drawing.Point(25,385)
$HWButton.Size = New-Object System.Drawing.Size(70,70)
$HWButton.BackgroundImage = $objImageHW
$HWButton.Add_Click( {
    Start-Process "C:\Program Files\CPUID\HWMonitor\HWMonitor.exe" -WorkingDirectory "C:\Program Files\CPUID\HWMonitor" -PassThru -WindowStyle Normal -ErrorAction Continue 
} )
$HWButton.FlatStyle = 1
$HWButton.DialogResult = 0
$HWButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $HWButton
$Form1.Controls.Add($HWButton)



# // Steam \\
$StButton = New-Object System.Windows.Forms.Button
$StButton.Location = New-Object System.Drawing.Point(1045,115)
$StButton.Size = New-Object System.Drawing.Size(70,70)
$StButton.BackgroundImage = $objImageSt
$StButton.Add_Click( {
    Start-Process "D:\Steam\steam.exe" -WorkingDirectory "D:\Steam" -WindowStyle Normal -ErrorAction Continue
} )
$StButton.FlatStyle = 1
$StButton.DialogResult = 0
$StButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $StButton
$Form1.Controls.Add($StButton)



# // Twitch \\
$TButton = New-Object System.Windows.Forms.Button
$TButton.Location = New-Object System.Drawing.Point(1045,295)
$TButton.Size = New-Object System.Drawing.Size(70,70)
$TButton.BackgroundImage = $objImageT
$TButton.Add_Click( {
    Start-Process "$env:APPDATA\Twitch\Bin\Twitch.exe" -WorkingDirectory "$env:APPDATA\Twitch\bin" -WindowStyle Normal -ErrorAction Continue
} )
$TButton.FlatStyle = 1
$TButton.DialogResult = 0
$TButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $TButton
$Form1.Controls.Add($TButton)



# // StreamLabs \\
$TButton = New-Object System.Windows.Forms.Button
$TButton.Location = New-Object System.Drawing.Point(1045,385)
$TButton.Size = New-Object System.Drawing.Size(70,70)
$TButton.BackgroundImage = $objImageSL
$TButton.Add_Click( {
    Start-Process "D:\StreamLabs\Streamlabs OBS\Streamlabs OBS.exe" -WorkingDirectory "D:\StreamLabs\Streamlabs OBS" -WindowStyle Normal -ErrorAction Continue
} )
$TButton.FlatStyle = 1
$TButton.DialogResult = 0
$TButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $TButton
$Form1.Controls.Add($TButton)



# // SUITE \\
$SUITEButton = New-Object System.Windows.Forms.Button
$SUITEButton.Location = New-Object System.Drawing.Point(1045,565)
$SUITEButton.Size = New-Object System.Drawing.Size(70,70)
$SUITEButton.Text = "Page suivante"
$SUITEButton.ForeColor = "White"
$SUITEButton.BackgroundImage = $objImageSUITE
$SUITEButton.Add_Click( {
    ####
    #fermeture Page 1
    $Form1.Dispose() | Out-Null -ErrorAction Ignore
    $Form1.Close() | Out-Null -ErrorAction Ignore
    
    #ouverture Page 2
    Add-Type -AssemblyName System.Windows.Forms
    Add-Type -AssemblyName System.Drawing

    $Form2 = New-Object System.Windows.Forms.Form
    $Form2.Text = " LauncherOfGamer"
    $Form2.Size = New-Object System.Drawing.Size(1152,730)
    $Form2.ShowInTaskbar = $true
    $Form2.StartPosition = "CenterScreen"
    $Form2.MinimizeBox = $true
    $Form2.MaximizeBox = $false
    $Form2.ControlBox = $true
    $Form2.FormBorderStyle = 3
    $Form2.Opacity = 1
    $Form2.UseWaitCursor = $false
    $Form2.AutoScroll = $false
    $Form2.HorizontalScroll.Enabled = $false
    $Form2.VerticalScroll.Enabled = $false
    $Form2.VerticalScroll.Visible = $false
    $Form2.Topmost = $false
    $Form2.MaximumSize = "1152,730" 
    $Form2.MinimumSize = "1152,730"
    $Form2.SizeGripStyle = 2
    $Form2.Capture = $false
    $Form2.KeyPreview = $false

    $ScriptDir = $PSScriptRoot
    
    $Form2.BackgroundImage = $objImage

    $objIcon = New-Object system.drawing.icon ("$ScriptDir\Icones\Logo.ico")
    $Form2.Icon = $objIcon

    #Gpedit
    $GEButton = New-Object System.Windows.Forms.Button
    $GEButton.Location = New-Object System.Drawing.Point(25,25)
    $GEButton.Size = New-Object System.Drawing.Size(70,70)
    $GEButton.BackgroundImage = $objImageGE
    $GEButton.Add_Click( {
        Start-Process "D:\TOUT\Scripts\DOS\gpedit.bat" -WorkingDirectory "D:\TOUT\Scripts\DOS" -PassThru -WindowStyle Normal -ErrorAction Continue
    } )
    $GEButton.FlatStyle = 1
    $GEButton.DialogResult = 0
    $GEButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $GEButton
    $Form2.Controls.Add($GEButton)

    #Regedit
    $REButton = New-Object System.Windows.Forms.Button
    $REButton.Location = New-Object System.Drawing.Point(25,115)
    $REButton.Size = New-Object System.Drawing.Size(70,70)
    $REButton.BackgroundImage = $objImageRE
    $REButton.Add_Click( {
        Start-Process "D:\TOUT\Scripts\DOS\Regedit.bat" -WorkingDirectory "D:\TOUT\Scripts\DOS" -PassThru -WindowStyle Normal -ErrorAction Continue
    } )
    $REButton.FlatStyle = 1
    $REButton.DialogResult = 0
    $REButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $REButton
    $Form2.Controls.Add($REButton)

    #OF
    $OFButton = New-Object System.Windows.Forms.Button
    $OFButton.Location = New-Object System.Drawing.Point(25,205)
    $OFButton.Size = New-Object System.Drawing.Size(70,70)
    $OFButton.BackgroundImage = $objImageOF
    $OFButton.Add_Click( {
        Start-Process "$env:WINDIR\System32\OptionalFeatures.exe" -WorkingDirectory "$env:WINDIR\System32" -PassThru -WindowStyle Normal -ErrorAction Continue
    } )
    $OFButton.FlatStyle = 1
    $OFButton.DialogResult = 0
    $OFButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $OFButton
    $Form2.Controls.Add($OFButton)


    #Steel
    $SteelButton = New-Object System.Windows.Forms.Button
    $SteelButton.Location = New-Object System.Drawing.Point(25,295)
    $SteelButton.Size = New-Object System.Drawing.Size(70,70)
    $SteelButton.BackgroundImage = $objImageSteel
    $SteelButton.Add_Click( {
        Start-Process "C:\Program Files\SteelSeries\SteelSeries Engine 3\SteelSeriesGG.exe" -ArgumentList '-dataPath="C:\ProgramData\SteelSeries\SteelSeries Engine 3" -dbEnv=production' -WorkingDirectory "C:\Program Files\SteelSeries\SteelSeries Engine 3" -WindowStyle Normal -ErrorAction Continue
    } )
    $SteelButton.FlatStyle = 1
    $SteelButton.DialogResult = 0
    $SteelButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $SteelButton
    $Form2.Controls.Add($SteelButton)


    #ShortCut
    $SCButton = New-Object System.Windows.Forms.Button
    $SCButton.Location = New-Object System.Drawing.Point(25,385)
    $SCButton.Size = New-Object System.Drawing.Size(70,70)
    $SCButton.BackgroundImage = $objImageSC
    $SCButton.Add_Click( {
        Start-Process "C:\Program Files\Shotcut\shotcut.exe" -WorkingDirectory "C:\Program Files\Shotcut" -WindowStyle Normal -ErrorAction Continue
    } )
    $SCButton.FlatStyle = 1
    $SCButton.DialogResult = 0
    $SCButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $SCButton
    $Form2.Controls.Add($SCButton)

    #GTA V
    $GTAButton = New-Object System.Windows.Forms.Button
    $GTAButton.Location = New-Object System.Drawing.Point(125,25)
    $GTAButton.Size = New-Object System.Drawing.Size(285,165)
    $GTAButton.BackgroundImage = $objImageGTA
    $GTAButton.Add_Click( {
        Start-Process 'D:\Rockstar\Launcher\LauncherPatcher.exe' -WorkingDirectory 'D:\Rockstar\Launcher' -WindowStyle Normal -ErrorAction Continue
        Start-Sleep 5
        Start-Process 'S:\GTA\Grand Theft Auto V\PlayGTAV.exe' -WorkingDirectory 'S:\GTA\Grand Theft Auto V' -WindowStyle Normal -ErrorAction Continue
    } )
    $GTAButton.FlatStyle = 1
    $GTAButton.DialogResult = 0
    $GTAButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $GTAButton
    $Form2.Controls.Add($GTAButton)


    #Golf-it!
    $GLFButton = New-Object System.Windows.Forms.Button
    $GLFButton.Location = New-Object System.Drawing.Point(427,25)
    $GLFButton.Size = New-Object System.Drawing.Size(285,165)
    $GLFButton.BackgroundImage = $objImageGLF
    $GLFButton.Add_Click( {
        Start-Process 'D:\Steam\steam.exe' -WorkingDirectory 'D:\Steam' -WindowStyle Normal -ErrorAction Continue
        Start-Process 'steam://rungameid/571740' -ErrorAction Continue
    } )
    $GLFButton.FlatStyle = 1
    $GLFButton.DialogResult = 0
    $GLFButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $GLFButton
    $Form2.Controls.Add($GLFButton)


    #R6
    $R6Button = New-Object System.Windows.Forms.Button
    $R6Button.Location = New-Object System.Drawing.Point(730,25)
    $R6Button.Size = New-Object System.Drawing.Size(285,165)
    $R6Button.BackgroundImage = $objImageR6
    $R6Button.Add_Click( {
        Start-Process 'D:\Steam\steam.exe' -WorkingDirectory 'D:\Steam' -WindowStyle Normal -ErrorAction Continue
        Start-Process 'D:\Ubisoft\Ubisoft Game Launcher\UbisoftConnect.exe' -WorkingDirectory 'D:\Ubisoft\Ubisoft Game Launcher' -WindowStyle Normal -ErrorAction Continue
        Start-Sleep -Seconds 3
        Start-Process 'steam://rungameid/359550' -ErrorAction Continue
    } )
    $R6Button.FlatStyle = 1
    $R6Button.DialogResult = 0
    $R6Button.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $R6Button
    $Form2.Controls.Add($R6Button)


    #Apex Legends
    $ALButton = New-Object System.Windows.Forms.Button
    $ALButton.Location = New-Object System.Drawing.Point(730,200)
    $ALButton.Size = New-Object System.Drawing.Size(285,165)
    $ALButton.BackgroundImage = $objImageAL
    $ALButton.Add_Click( {
        Start-Process 'D:\Steam\steam.exe' -WorkingDirectory 'D:\Steam' -WindowStyle Normal -ErrorAction Continue
        Start-Process 'steam://rungameid/1172470' -ErrorAction Continue
    } )
    $ALButton.FlatStyle = 1
    $ALButton.DialogResult = 0
    $ALButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $ALButton
    $Form2.Controls.Add($ALButton)


    #RimWorld
    $RWButton = New-Object System.Windows.Forms.Button
    $RWButton.Location = New-Object System.Drawing.Point(427,200)
    $RWButton.Size = New-Object System.Drawing.Size(285,165)
    $RWButton.BackgroundImage = $objImageRW
    $RWButton.Add_Click( {
        Start-Process 'D:\Steam\steam.exe' -WorkingDirectory 'D:\Steam' -WindowStyle Normal -ErrorAction Continue
        Start-Process 'steam://rungameid/294100' -ErrorAction Continue
    } )
    $RWButton.FlatStyle = 1
    $RWButton.DialogResult = 0
    $RWButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $RWButton
    $Form2.Controls.Add($RWButton)



    #About
    $AboutButton = New-Object System.Windows.Forms.Button
    $AboutButton.Location = New-Object System.Drawing.Point(1045,25)
    $AboutButton.Size = New-Object System.Drawing.Size(70,70)
    $AboutButton.BackgroundImage = $objImageAbout
    $AboutButton.Add_Click({About})
    $AboutButton.FlatStyle = 1
    $AboutButton.DialogResult = 0
    $AboutButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $AboutButton
    $Form2.Controls.Add($AboutButton)



    #Dessin Draw
    $DrawButton = New-Object System.Windows.Forms.Button
    $DrawButton.Location = New-Object System.Drawing.Point(1045,115)
    $DrawButton.Size = New-Object System.Drawing.Size(70,70)
    $DrawButton.BackgroundImage = $objImageDraw
    $DrawButton.Add_Click({Draw})
    $DrawButton.FlatStyle = 1
    $DrawButton.DialogResult = 0
    $DrawButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $DrawButton
    $Form2.Controls.Add($DrawButton)
    


    #Retour / Back
    $BackButton = New-Object System.Windows.Forms.Button
    $BackButton.Location = New-Object System.Drawing.Point(25,565)
    $BackButton.Size = New-Object System.Drawing.Size(70,70)
    $BackButton.Text = "Page précédente"
    $BackButton.ForeColor = "White"
    $BackButton.BackgroundImage = $objImageBack
    $BackButton.Add_Click( {
       
       #Fermeture Form2
       $NotifyIcon.Dispose() | Out-Null -ErrorAction Ignore

       $Form2.Dispose() | Out-Null -ErrorAction Ignore
       $Form2.Close() | Out-Null -ErrorAction Ignore

       #Ouverture Form1
       PowerShell.exe -windowstyle hidden "D:\TOUT\Scripts\PowerShell\Launcher\Launcher.ps1"

    } )
    $BackButton.FlatStyle = 1
    $BackButton.DialogResult = 0
    $BackButton.UseVisualStyleBackColor = $true
    $Form2.AcceptButton = $BackButton
    $Form2.Controls.Add($BackButton)
    
    $result = $Form2.ShowDialog()
    ####
    } )
$SUITEButton.FlatStyle = 1
$SUITEButton.DialogResult = 0
$SUITEButton.UseVisualStyleBackColor = $true
$Form1.AcceptButton = $SUITEButton
$Form1.Controls.Add($SUITEButton)

# // Notif + lancement \\
$Notif = New-BurntToastNotification -Text "LauncherOfGamer", "Le launcher s'est bien lancé !" -AppLogo "$ScriptDir\Icones\RondVert.ico" -Sound Mail

$result = $Form1.ShowDialog()

Write-Host "La date : $Date" -ErrorAction SilentlyContinue

# Fin des logs
Stop-Transcript -ErrorAction Ignore
