﻿Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

$Form3 = New-Object System.Windows.Forms.Form
$Form3.Text = " LauncherOfGamer"
$Form3.Size = New-Object System.Drawing.Size(400,150)
$Form3.ShowInTaskbar = $true
$Form3.StartPosition = "CenterScreen"
$Form3.MinimizeBox = $true
$Form3.MaximizeBox = $false
$Form3.ControlBox = $true
$Form3.FormBorderStyle = 3
$Form3.Opacity = 1
$Form3.UseWaitCursor = $false
$Form3.AutoScroll = $false
$Form3.HorizontalScroll.Enabled = $false
$Form3.VerticalScroll.Enabled = $false
$Form3.VerticalScroll.Visible = $false
$Form3.Topmost = $false
$Form3.MaximumSize = "400,150" 
$Form3.MinimumSize = "400,150"
$Form3.SizeGripStyle = 2
$Form3.Capture = $false
$Form3.KeyPreview = $false
$Form3.AllowTransparency = $false
$Form3.AllowDrop = $false
$Form3.BackColor = "Black"
$objIcon = New-Object system.drawing.icon ("D:\TOUT\Scripts\PowerShell\Launcher\icones\Logo.ico")
$Form3.Icon = $objIcon

$image3 = New-Object System.Windows.Forms.PictureBox
$image3.Image = [system.drawing.image]::FromFile("D:\TOUT\Scripts\PowerShell\Launcher\Images\Repair.png")
$image3.Size = New-Object System.Drawing.Size(64,64)
$image3.Location = New-Object System.Drawing.Point(290,5)
$Form3.Controls.Add($image3)


$label3 = New-Object System.Windows.Forms.Label
$label3.Location = New-Object System.Drawing.Point(0,0)
$label3.Size = New-Object System.Drawing.Size(250,60)
$label3.Text = "Réparation en cours..."
$label3.ForeColor = "White"
$Form3.Controls.Add($label3)

$progressBar1 = New-Object System.Windows.Forms.ProgressBar
$progressBar1.Name = 'progressBar1'
$progressBar1.Value = 0
$progressBar1.Style="Continuous"
$progressBar1.Size = New-Object System.Drawing.Size(400,40)
$progressBar1.Location = New-Object System.Drawing.Point(0,70)

$Form3.Controls.Add($progressBar1)
$Form3.Show()| Out-Null
$Form3.Focus() | Out-Null
$Form3.Refresh()
$i=-1

while($i -lt 100) {
    Start-Sleep -Milliseconds 30
    $i++
    Write-Host "$i"
    $progressbar1.Value = $i
    $progressBar1.Refresh() | Out-Null
    $progressBar1.Focus() | Out-Null
    $Form3.Refresh() | Out-Null
    if($i -eq 25) {
        $label3.Text = "Réparation en cours..."
        $label3.Refresh()
    }
    if($i -eq 45) {
        $label3.Text = "Réparation en cours... `nRéparation du script Powershell..."
        $label3.Refresh()
    }
    if($i -eq 65) {
        $label3.Text = "Réparation en cours... `nRéparation du script Powershell... `nCopie des fichiers Powershell de secours..."
        $label3.Refresh()
    }
    if($i -eq 85) {
        $label3.Text = "Réparation en cours... `nRéparation du script Powershell... `nCopie des fichiers Powershell de secours... `nSuppression des fichiers redondants..."
        $label3.Refresh()
    }
    if($i -eq 95) {
        $label3.Text = "Réparation en cours... `nRéparation du script Powershell... `nCopie des fichiers Powershell de secours... `nSuppression des fichiers redondants... `nFinalisation de la réparation..."
        $label3.Refresh()
        Start-Sleep 1
    }
}
$Form3.Refresh() | Out-Null
$Form3.Dispose() | Out-Null
$Form3.Close() | Out-Null